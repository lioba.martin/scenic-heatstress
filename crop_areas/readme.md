## Crop Area Information for SCENIC Heatstress calculations

`nextcloud/lioba_andy_shared_folder/harvested_areas`

-   Harvested Area per crop `harvested_areas_2000-2021.nc` (based on the MIRCA2000 dataset)
    -   This is produced using the notebook `harvested_areas.ipynb`, which also has code for how to open the MIRCA2000 dataset. (If you want to include more crops or go to lower resolution then the MIRCA2000 dataset is here: <https://zenodo.org/record/7422506> -\> download 'harvested_areas_grids.zip')
    
-   `winter_and_spring_wheat_areas_phase3.nc4` division winter wheat / spring wheat as used in the GGCMI Project
    -   Rule in `spring:winter_wheat_rule.png`
