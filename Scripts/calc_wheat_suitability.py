import numpy as np
#import python modules
import xarray as xr
import time
import glob
import logging
import sys
import os


# import local modules
import home
import helperFunctions as hf
import dicts


start = time.time() #take time

#set logging configurations 
#logging.basicConfig(format='%(asctime)s | %(levelname)s: %(message)s',filename=f"{home.hshome}/Slurm/wsm_{sys.argv[1]}.out",level = logging.INFO,filemode="w")
logging.basicConfig(format='%(asctime)s | %(levelname)s: %(message)s',stream=sys.stdout,level = logging.INFO)

#add basic infos
crop = "winter_wheat"
forcing = sys.argv[1]
res = "025"
watering = "rf"
chill_required = dicts.crop_dict[crop]["chill_units"]
gdd_required = dicts.crop_dict[crop]["gdd_maturity"]
#chill_importance: weighting of the chill factor for growing suitability 
chill_importance = 0.7

GDD_file = sorted(glob.glob(f"{home.hspath}/HI_final/ggcmi_planting/scenic_EU{res}_{forcing}_GDD_{crop}_{watering}*.nc"), key=os.path.getmtime)[-1]

mode = "run"

logging.info(f"running {sys.argv[0]} for {forcing} in mode {mode}")

s_lon = []
s_list = []
ds = xr.open_dataset(GDD_file)
ds = ds.where(ds.mean(dim="date").mask > 0)[["achill","aGDD"]]
if mode == "test":
    if (res == "025"):
        #lat_vals = [50.125, 50.375, 50.625, 50.875]
        #lon_vals = [8.125, 8.375, 8.625, 8.875]
        lat_vals = [61.375, 61.625, 61.875,62.125]
        lon_vals = [6.125,6.375, 6.625, 6.875]
        #error at 6.875 62.125
    if (res == "011"):
        lat_vals = [40.485 , 40.5961, 40.7072, 40.8183, 40.9294]
        lon_vals = [23.07  , 23.1813, 23.2926, 23.4039, 23.5152]
elif mode == "int":
    ds = ds.sel(lat=slice(40,50),lon=slice(0,8))
    lat_vals =  ds.lat.values
    lon_vals =  ds.lon.values
else: 
    lat_vals =  ds.lat.values
    lon_vals =  ds.lon.values
logging.info(f"calculating achill for \nlats:{lat_vals},\nlons:{lon_vals}")
for lat in lat_vals:
    for lon in lon_vals:
        cell = ds.sel(lat=lat,lon=lon)
        cell["year"] = cell.date.dt.year
        cell_y = cell.groupby("year").max()
        cold_winters = sum(cell_y.achill >  chill_required* chill_importance)
        if cold_winters> 0:
            suitability_cell = cold_winters   
        elif ((sum(cell_y.achill > 0)== 0) | (cell_y.aGDD.mean() < 1000)):
            suitability_cell = np.nan
            logging.debug("crop can't grow")
        else:
            suitability_cell = 0
        if ((sum(cell_y.achill >= 120)) >= 2):
            suitability_cell = 7
            logging.debug(f"too cold in cell {lat},{lon}: {(sum(cell_y.achill >= 120)) >= 2} long winters")
        s_lon.append(suitability_cell)
    s_list.append(s_lon)
    s_lon = []
logging.info("done")
	
#create empty array 
lat_array = xr.DataArray(lat_vals,dims="lat", name = "lat")
lon_array = xr.DataArray(lon_vals,dims="lon", name = "lon")
coords = {"lat":lat_array,"lon":lon_array}
# pass nested list to array 
wheat_map = xr.DataArray(s_list,
                  dims=["lat","lon"], 
                  coords=coords)
# add attributes
wheat_map.attrs={"description":"number of winters between 2017 - 2022 with sufficient chill units for growing winter wheat",
			"forcing":forcing,
			"chill importance":chill_importance,
			 "grid resolution":int(res)/100,
			 "creation date":time.strftime('%Y-%m-%d %H:%M:%S',time.localtime()),
			 "script":sys.argv[0],
			 "contact":home.email}
wheat_map.lat.attrs={'standard_name': 'latitude',
 'long_name': 'latitude',
 'units': 'degrees_north',
 'axis': 'Y'}
wheat_map.lon.attrs={'standard_name': 'longitude',
 'long_name': 'longitude',
 'units': 'degrees_east',
 'axis': 'X'}

logging.info(f"saving {home.hspath}/crop_areas/wwh_planting_suitability_{forcing}_{res}_ci{str(chill_importance).replace('.','')}.nc")
wheat_map.to_netcdf(f"{home.hspath}/crop_areas/wwh_planting_suitability_{forcing}_{res}_ci{str(chill_importance).replace('.','')}.nc")

# plot the output
logging.info("plotting map")

import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import cartopy
import cartopy.crs as ccrs

#make colorbar
color_list = ["darkgrey","darkgrey","lightgrey","bisque","wheat","gold","orange","sandybrown","aliceblue"]
color_obj = mcolors.LinearSegmentedColormap.from_list("mymap",color_list,8)
# set the boundaryies for the colors (+-0.5 around the value)
colorvalues = np.linspace(-1.5,7.5,10)
#assign data
data = wheat_map
fig,ax = plt.subplots(figsize=(5, 5),subplot_kw={"projection": ccrs.Orthographic(5,15)})
plot = ax.contourf(data.lon,data.lat,data,transform=ccrs.PlateCarree(),cmap = color_obj,levels =colorvalues)
#add coastlines
ax.coastlines();
# add borders
ax.add_feature(cartopy.feature.BORDERS)
#add grid values without lines left and botttom
ax.gridlines(visible=False,draw_labels={"bottom": "x", "left": "y"})
#add colorbar
cbar_ax = fig.add_axes([0.95, 0.15, 0.05, 0.7])
cbar = fig.colorbar(plot,cax=cbar_ax,pad=0.5)
cbar.set_label('wheat suitability',  rotation=90,fontsize=10)
cbar.set_ticks(np.linspace(-1,7,9)) #place the labels in the middle of the values
cbar.ax.tick_params(labelsize=10)
ax.set_title(forcing)
# save figure 
plt.savefig(f"{home.hshome}/Plots/crop_areas/wwh_planting_suitability_{forcing}_{res}_ci{str(chill_importance).replace('.','')}.png",bbox_inches="tight")

end = time.time()
logging.info(f"ran for {(end-start)/60}")


