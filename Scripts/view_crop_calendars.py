# -*- coding: utf-8 -*-
"""
Created on Thu Jun  1 16:04:41 2023

@author: martin-l
"""
import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import cartopy
import home
#%% open and plot
ds = xr.open_dataset(f"{home.datapath}/smerald-a/resources/crop_calendars/wwh_rf_ggcmi_crop_calendar_phase3_v1.01.nc4")

inc_lat_orig = ds.lat.values[0]-ds.lat.values[1]
inc_lon_orig = ds.lon.values[1]-ds.lon.values[0]
print(f"Original Increment = {inc_lat_orig}° Lat")
print(f"Original Increment = {inc_lon_orig}° Lon")
#%% 
data = ds.planting_day


fig, ax = plt.subplots(subplot_kw={"projection": ccrs.Orthographic(4,15)})
plot = ax.contourf(ds.lon,ds.lat,ds.planting_day,transform=ccrs.PlateCarree())
cbar = plt.colorbar(plot,pad=0.05)
cbar.set_label('doy', labelpad=10,  rotation=90,fontsize=20)
cbar.ax.tick_params(labelsize=16)
cbar.minorticks_on()
ax.coastlines();

ax.set_title("Original Resolution ")
ax.add_feature(cartopy.feature.BORDERS)
plt.show()


#%% open Mask 

mask = xr.open_dataset("mask_EU011_400sec.nc")

dfmask = mask.to_dataframe().dropna().reset_index()
mask.lat.values
print(f"lat0 = {mask.lat.values[0]}, lat133 = {mask.lat.values[len(mask.lat)-1]}, lenght = {len(mask.lat.values)}, inc = {mask.lat.values[1]-mask.lat.values[0]}")
print(f"lon0 = {mask.lon.values[0]}, lon168 = {mask.lon.values[len(mask.lon)-1]}, lenght = {len(mask.lon.values)}, inc = {mask.lon.values[1]-mask.lon.values[0]}")
#print(mask.lat.values)




#%%

ds_eu = ds.sel(lat=slice(mask.lat.values[len(mask.lat)-1],mask.lat.values[0])).sel(lon=slice(mask.lon.values[0],mask.lon.values[len(mask.lon)-1]))

data = ds_eu.planting_day

fig, ax = plt.subplots(subplot_kw={"projection": ccrs.Orthographic(4,15)})
plot = ax.contourf(data.lon,data.lat,data,transform=ccrs.PlateCarree())
cbar = plt.colorbar(plot,pad=0.05)
cbar.set_label('doy', labelpad=10,  rotation=90,fontsize=20)
cbar.ax.tick_params(labelsize=16)
cbar.minorticks_on()
ax.coastlines();

ax.set_title("Original Resolution Europe ")
ax.add_feature(cartopy.feature.BORDERS)

#%%
#bash
#sh Scripts/cdo_remap_crop_calendars.sh

#%% 

#%% open and plot regridded 0.11
ds = xr.open_dataset(f"{home.homepath}/scenic_heatstress/crop_calendar/GGCMI_crop_calendars_EU011/wwh_rf_ggcmi_crop_calendar_EU011.nc4")

#print(ds.data_source_used.long_name)


#%% 


fig, ax = plt.subplots(subplot_kw={"projection": ccrs.Orthographic(4,15)})
plot = ax.contourf(ds.lon,ds.lat,ds.planting_day,transform=ccrs.PlateCarree())
cbar = plt.colorbar(plot,pad=0.05)
cbar.set_label('doy', labelpad=10,  rotation=90,fontsize=20)
cbar.ax.tick_params(labelsize=16)
cbar.minorticks_on()
ax.coastlines();
ax.add_feature(cartopy.feature.BORDERS)
ax.set_title("Resolution 0.11° rainfed")

#%% open and plot regridded rainfed 0.25
ds_rf = xr.open_dataset(f"{home.homepath}/scenic_heatstress/crop_calendar/GGCMI_crop_calendars_EU025/wwh_rf_ggcmi_crop_calendar_EU025.nc4")

#print(ds_rf.data_source_used.long_name)


#%% 


fig, ax = plt.subplots(subplot_kw={"projection": ccrs.Orthographic(4,15)})
plot = ax.contourf(ds_rf.lon,ds_rf.lat,ds_rf.planting_day,transform=ccrs.PlateCarree())
cbar = plt.colorbar(plot,pad=0.05)
cbar.set_label('doy', labelpad=10,  rotation=90,fontsize=20)
cbar.ax.tick_params(labelsize=16)
cbar.minorticks_on()
ax.coastlines();
ax.add_feature(cartopy.feature.BORDERS)
ax.set_title("Resolution 0.25° rainfed")



#%% open and plot regridded irrigated 0.25
ds_ir = xr.open_dataset(f"{home.homepath}/scenic_heatstress/crop_calendar/GGCMI_crop_calendars_EU025/wwh_ir_ggcmi_crop_calendar_EU025.nc4")

#print(ds_ir.data_source_used.long_name)


#%% 




fig, ax = plt.subplots(subplot_kw={"projection": ccrs.Orthographic(4,15)})
plot = ax.contourf(ds_ir.lon,ds_ir.lat,ds_ir.planting_day,transform=ccrs.PlateCarree())
cbar = plt.colorbar(plot,pad=0.05)
cbar.set_label('doy', labelpad=10,  rotation=90,fontsize=20)
cbar.ax.tick_params(labelsize=16)
cbar.minorticks_on()
ax.coastlines();
ax.add_feature(cartopy.feature.BORDERS)
ax.set_title("Resolution 0.25° irrigated")

## difference between rainfed and irrigated

ds = ds_ir.planting_day - ds_rf.planting_day


fig, ax = plt.subplots(subplot_kw={"projection": ccrs.Orthographic(4,15)})
plot = ax.contourf(ds.lon,ds.lat,ds,transform=ccrs.PlateCarree(),cmap="RdBu_r",levels = np.linspace(-100,100,11))
cbar = plt.colorbar(plot,pad=0.05)
cbar.set_label('doy', labelpad=10,  rotation=90,fontsize=20)
cbar.ax.tick_params(labelsize=16)
cbar.minorticks_on()
ax.coastlines();
ax.add_feature(cartopy.feature.BORDERS)
ax.set_title("Resolution 0.25° irrigated - rainfed")
