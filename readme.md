## Analyze Heatstress in the SCENIC data using the GLAM Approach


Data: 

- Scenic Data: SCENIC at 0.25° (available upon request) 
  
- Crop Calendar: GGCMI (Jaegermayr et al 2021) 

Variables Used:

| Post-Processing Name | Description                                      |
|-----------------------|--------------------------------------------------|
| ASOB_S               | SW net flux at surface                          |
| ASODIFU_S            | SW upward flux at surface                       |
| ATHB_S               | LW net flux at surface                          |
| ATHU_S               | LW upward flux at surface                       |
| ARELHUM_2M           | Relative humidity in 2m                         |
| APMSL                | Mean sea level pressure                         |
| AT_2M                | Temperature in 2m                               |
| AQV_S                | Specific humidity at the surface                |
| ASODIFD_S            | SW diffuse downward flux at surface             |
| ASOD_S               | Total downward SW radiation at surface          |
| ATHD_S               | Total downward LW radiation at surface          |
| FR_SNOW              | Snow cover fraction                             |
| SMI                  | Soil moisture index                             |
| T_SO                 | Weighted soil temperature (main level)          |
| TOT_PREC             | Total precipitation                             |


(FR_SNOW, SMI & T_SO are read in seperatly as they were added later for the dynamic planting date calculation)

- Scripts to calculate GDD + flowering, and the anthesis heatstress (`Scripts`) 

