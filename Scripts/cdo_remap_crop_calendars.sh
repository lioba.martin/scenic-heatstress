#!/bin/bash/
#
module load openmpi/4.1.4-gcc-11.3.1-cuda
module load cdo/2.1.0-gcc-11.3.1-ompi-mkl-cuda

crops="bar mai rap rye swh wwh" 
resolution="025"
water="ir"


# Reproject crop calendars
for crop in $crops
do 
    echo $crop
    file=$(find GGCMI_crop_calendars.ln/  -name "*"$water"_ggcmi_crop_calendar_phase3_v1.01.nc4" | grep $crop )
    echo $file
    cdo -remapnn,"scenic_EU"$resolution".ln/gridfile_EU"$resolution".txt" $file "./GGCMI_crop_calendars_EU"$resolution"/"$crop"_"$water"_ggcmi_crop_calendar_EU"$resolution".nc4"
    echo $crop"_"$water"_ggcmi_crop_calendar_EU"$resolution".nc4"
done 
