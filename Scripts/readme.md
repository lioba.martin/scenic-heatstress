## Scripts for Analyzing the Heatstress in the Scenic

For this heat stress analysis, the method by Moriondo, as applied in Teixeira et al. should be tested using the 
Scenic Data

  - `home.py`: Script containing all base paths etc for application on different systems
  - `View_CropCalendars.py`: First look and plots at the crop calendars
  - Scripts for the creation of the Mask: This should in principle be done in the raw data folder, reprojecting to the mask used. Therefore a duplicate. 
  	- `eurnuts0.shp` Shapefile for the Maks
	- `createMask.py`: Creates the Masks based on the shapefile
	-  `geohash_code.py`: Helper script for createMask.py
	- `gridfile_EU011.txt`: Gridfile for the CDO operation `cdo remapnn`
	- `mask_EU011_400sec.nc`: The created mask as NetCDF
	- `mask_EU025.nc`: created mask in the given resulution again regridded using `cdo remapnn` (which is necessary because of digit errors in the original mask, works, but could probably be fixed in `createMask.py`
- `cdo_remap_crop_calendars.sh`: Script for Remapping the given crop calendars using CDO 
- `calcGDD.py` calculate the temperature sums
	- `dicts.py` dictionaries for the species parameters
- `p_calcGDD.py`version of calcGDD.py that runs in parrallel (takes ~45 minutes for 22344 Gridcells)
- `heatstress.py`


- `calc_wheat_suitability.py` calculate maps for could enough withers in the storylines 
- `calc_planting.py` calculate planting dates based on rules + scenic climate forcing 
