## Convert pandas dataframe with lat, lon to netcdf 
def csvToNetCDF(df, maskFileName, outputFileName):
    import xarray as xr
    import numpy as np
    import pandas as pd
   
    # The netCDF Outline including the ids
    ds = xr.open_dataset(maskFileName)
    # Create a Dataframe with the ids and the (lat,lon)
    dm = {}
    for ila, la in enumerate(ds.lat.values):
        for ilo, lo in enumerate(ds.lon.values):

            the_id = ds.ID[ila,ilo].values
            if np.isnan(the_id) == False:
                dm[int(the_id)] = (la,lo)
    #
    # Store the dm dictionary into data according to the id
    df["coords"] = df.ID.map(dm)
    # add lat and lon from the coords touple
    df[['lat', 'lon']] = pd.DataFrame(df['coords'].tolist(), index=df.index) 
    # Correct the datatime in the data Dataframe
    del df["coords"]
    data2 = df.copy(deep=True)
    data2 = data2.set_index(['date','lat','lon'])
    # Create an empty Dataset and fill it with data2 Dataframe and use index as coordinates. 
    dsout = xr.Dataset()
    dsout = dsout.from_dataframe(data2)
    # Save the new Dataset to a netCDF file.
    dsout.to_netcdf(outputFileName)
#

def dfToNetCDF(df,outputFileName):
	import xarray as xr
	dfx = df.set_index(["date","lat","lon"])
	dsx = xr.Dataset()
	dsx = dsx.from_dataframe(dfx)
	outputFileName = outputFileName + ".nc" 
	dsx.to_netcdf(outputFileName)
	return dsx

#function for making sure files are not being overwritten if the file exists, create a new filename
def catch_filenames(output_file,mode):
	import os
	if (mode == "test"):
		output_file = output_file + "_test"
	i=1
	while (os.path.isfile(output_file + ".nc" ) == True):
		output_file = output_file + f"_{i}"
		i += 1
	return output_file

def plotOverview(flowering_da,GDD_da):
    import xarray as xr
    import numpy as np
    import dicts
    import cartopy.crs as ccrs
    import cartopy
    import matplotlib.pyplot as plt
    import matplotlib.gridspec as gridspec
    import seaborn as sns
    print("plotting overview")
    fig = plt.figure(layout="constrained",figsize = (10,12))
    gs = gridspec.GridSpec(3, 2, figure=fig)
    ax1 = plt.subplot(gs[0,0],projection=ccrs.PlateCarree())
    ax2 = plt.subplot(gs[0,1],projection=ccrs.PlateCarree())
    ax3 = plt.subplot(gs[1,:])
    ax4 = plt.subplot(gs[2,:])

    plot = ax1.contourf(GDD_da.lon,GDD_da.lat,GDD_da.aGDD.max(dim="date"),levels=np.linspace(1,GDD_da.aGDD.max()),cmap="viridis")
    plt.colorbar(plot,orientation='horizontal')
    ax1.gridlines(draw_labels={"bottom": "x", "left": "y"}, dms=True, x_inline=False, y_inline=False,alpha=0.5, linestyle='--',linewidth=0.5)
    ax1.add_feature(cartopy.feature.COASTLINE)
    ax1.add_feature(cartopy.feature.BORDERS)
    ax1.set_title("maxGDD")
    
    ax2.gridlines(draw_labels={"bottom": "x", "left": "y"},dms=True,x_inline=False, y_inline=False,alpha=0.5, linestyle='--',linewidth=0.5)
    plot = ax2.contourf(flowering_da.lon,flowering_da.lat,flowering_da.flowering.max(dim="date"),levels=np.linspace(flowering_da.flowering.min(),flowering_da.flowering.max()),cmap="viridis")
    plt.colorbar(plot,orientation='horizontal')
    ax2.add_feature(cartopy.feature.COASTLINE)
    ax2.add_feature(cartopy.feature.BORDERS)
    ax2.set_title("floweringGDD")

    #ax3.scatter(flowering_da.mean(dim=["lat","lon"]).flowering)
    ax3.plot(GDD_da.date,GDD_da.mean(dim=["lat","lon"]).aGDD)
    ax3.set_title("meanGDD")
    
    sns.lineplot(ax=ax4,x = GDD_da.date.dt.dayofyear,y=GDD_da.isel(lat=60,lon=80).aGDD,hue = GDD_da.isel(lat=60,lon=80).date.dt.year,palette="viridis")
    #ax4.scatter(GDD_da.date.dt.dayofyear,GDD_da.isel(lat=60,lon=80).aGDD,c = GDD_da.isel(lat=60,lon=80).date.dt.year)
    ax4.set_title(f"cellGDD;{flowering_da.lat.values[60]},{flowering_da.lon.values[80]}")
    sns.scatterplot(ax=ax4,x=flowering_da.isel(lat=60,lon=80).flowering_date.dt.dayofyear,y=flowering_da.isel(lat=60,lon=80).flowering,hue = flowering_da.isel(lat=60,lon=80).flowering_date.dt.year,palette="viridis")
    fig.suptitle(f"f{scenario},{crop},{res},{watering}")
    plt.savefig(f"{home.oplt}/EU{res}_f{scenario}_{crop}_{watering}_overview.png",dpi=300,bbox_inches='tight');