# -*- coding: utf-8 -*-
"""
Created on Tue Jun  6 16:04:41 2023

@author: martin-l
"""

#%% read libraries
import rioxarray as rio
import xarray as xr
import geopandas as gpd 
import numpy as np

import os

import geohash_code as geohash
import local as local

#%% Setup gridfile for CDO 
digits = 100 

nenner = 9
resolution = f"{int(60/nenner*60)}sec"
res = 1/nenner

# nenner = local.nenner
# resolution = local.resolution
# res = local.res

print(nenner)

print(resolution)
print(res)

#'DE': ('Germany', (5.98865807458, 47.3024876979, 15.0169958839, 54.983104153)),
# ymin = 47.302
# ymax = 55.983
# xmin = 5.988
# xmax = 15.017
lat_max = 67.875
lat_min = 34.875
lon_min = -10.375
lon_max = 31.375
ymin = lat_min
ymax = lat_max
xmin = lon_min
xmax = lon_max


ysteps = int(((ymax - ymin ) / res ))
xsteps = int(((xmax - xmin ) / res ))


np.linspace
lats = np.linspace(ymin+0.5*res,ymax-0.5*res,num = ysteps)

lons = np.linspace(xmin+0.5*res,xmax-0.5*res,num = xsteps)

print(f"Lower left corner ( {lats[0]:.2f} / {lons[0]:.2f} ) to upper right corner  ( {lats[ysteps-1]:.2f} / {lons[xsteps-1]:.2f} ) ")



scale = 110 * res
print(f"Resolution: {1}/{nenner} ~ {res:.5f} deg, scale ~ {scale:.2f} (km)")
print(resolution)
ysteps = int(((ymax - ymin ) / res ))
xsteps = int(((xmax - xmin ) / res ))
gridcells = ysteps*xsteps
print()
print(f"latitude  {ysteps} cells")
print(f"longitude {xsteps} cells")
print()
print(f" {gridcells} Gridcells in total")
print(f" lats {(lats[0]):.5f}, {(lats[1]):.5f}, {(lats[2]):.5f}, {(lats[3]):.5f}, ...  ")
print(f" lats {(lons[0]):.5f}, {(lons[1]):.5f}, {(lons[2]):.5f}, {(lons[3]):.5f}, ...  ")

print(f" delta lat {(lats[1]-lats[0]):.5f}  ")

# Write Gridfile for CDO 
#lines = []
lines = f"gridtype=lonlat \nxsize = {xsteps} \nysize = {ysteps} \nxfirst = {lons[0]:.2f} \nxinc = {(lons[1]-lons[0]):.4f} \nyfirst = {(lats[0]):.2f} \nyinc = {(lats[1]-lats[0]):.4f}"
print(lines)
with open('gridfile_EU011.txt', 'w') as f:
    f.write(lines)
 
    
 
#%% Open Mask

ds = xr.DataArray(np.ones([ysteps,xsteps]), coords=[("lat",lats), ("lon", lons)] )
ds.rio.set_spatial_dims("lon","lat", inplace=True )    
 
gdf = gpd.read_file("Mask/eurnuts0.shp")
gdf.set_crs(epsg=4326, inplace=True)

ds = ds.rio.write_crs(gdf.crs)
ds.rio.crs

ds = ds.rio.clip(gdf.geometry, drop=False)
ds = ds.to_dataset(name="mask")
ds.mask.plot()

#%% add IDs using Geohash 
ids = ( ((ds.lat*digits).astype("int") )*100 * digits  + ((ds.lon*digits).astype("int") ) )
ds["id"] =  ids.where( ds.mask > 0 , 0)
ds["id"] = ids

# 
print("ids & mask")

ds["id"] = xr.ones_like(ds.id)
ds["id"].where( ds.mask > 0 ).plot()
#%%

ds1 = xr.DataArray(
    dims=["lat", "lon"],
    coords={"lat": ds.lat, "lon": ds.lon},
)

maxnum = len(ds.lat) * len(ds.lon)
#bar = progressbar.ProgressBar(maxval=maxnum).start() 
#bar.start()
cnt = 0
for j,lat in enumerate(ds.lat):
    for i,lon in enumerate(ds.lon):
        if ds.mask.sel(lat=lat, lon=lon) > 0 :
            id = geohash.coords2geohash_dec(lat=lat, lon=lon)
            print(lat.values, lon.values, id)
            ds1.loc[dict(lat=lat.values.item(), lon=lon.values.item())] = id
            #bar.update( cnt )
            cnt += 1
        else:
            continue
       
            
ds1.plot()

ds["id"] = ds1 
ds.id.plot()

ds.to_netcdf(f"mask_EU011_{resolution}.nc")
print(f"mask_EU011_{resolution}.nc")
