
#dictionary of crop names and parameters
crop_dict = {
    'spring_wheat' : {
        'ecosystem_type' : 'arable',
        'ldndc_crop_parent' : ':SPRINGWHEAT:',
        'ldndc_crop_mnemonic' : 'SPWH',
        'ldndc_crop_name' : 'Spring Wheat',
        'GGCMI_crop_short' : 'swh',
        'GGCMI_crop_fert' : 'whe',
        'Tbase' : 0,
        'Tmax' : 40,
        'vernalisation_flag' : 0,
        'gdd_maturity' : 2200,
        'gdd_flowering' : 1200,
        't_crit_min':25,
    },

    'winter_wheat' : {
        'ecosystem_type' : 'arable',
        'ldndc_crop_parent' : ':WINTERWHEAT:',
        'ldndc_crop_mnemonic' : 'WIWH',
        'ldndc_crop_name' : 'Winter Wheat',
        'GGCMI_crop_short' : 'wwh',
        'GGCMI_crop_fert' : 'whe',
        'Tbase' : 0,
        'Tmax' : 40,
        'vernalisation_flag' : 1,
        'gdd_maturity' : 2200,
        'gdd_flowering' : 1200,
        'chill_temp_max' : 5,
        'chill_units' : 35,
        't_crit_min':25,
    },

    'maize' : {
        'ecosystem_type' : 'arable',
        'ldndc_crop_parent' : ':CORN:',
        'ldndc_crop_mnemonic' : 'FOCO',
        'ldndc_crop_name' : 'Maize',
        'GGCMI_crop_short' : 'mai',
        'GGCMI_crop_fert' : 'mai',
        'gdd_maturity' : 1560,
        'gdd_flowering' : 860, 
        'Tbase' : 8,
        'Tmax' : 40,
        'vernalisation_flag' : 0,
        't_crit_min':32,
    },

    'maize_trop' : {
        'ecosystem_type' : 'arable',
        'ldndc_crop_parent' : ':FOODCORN:',
        'ldndc_crop_mnemonic' : 'FOCOTROPICAL',
        'ldndc_crop_name' : 'Maize_trop',
        'GGCMI_crop_short' : 'mai',
        'GGCMI_crop_fert' : 'mai',
        'Tbase' : 10,
        'Tmax' : 40,
        'vernalisation_flag' : 0,
        'Nsites' : 66621
    },

    'rice1_rf' : {
        'ecosystem_type' : 'arable',
        'ldndc_crop_parent' : ':INDICA:',
        'ldndc_crop_mnemonic' : 'SAHODULAN1',
        'ldndc_crop_name' : 'Rice',
        'GGCMI_crop_short' : 'ri1',
        'GGCMI_crop_fert' : 'ric',
        'Tbase' : 8,
        'Tmax' : 40,
        'vernalisation_flag' : 0,
        'Nsites' : 66621
    },

    'rice1_ir' : {
        'ecosystem_type' : 'arable',
        'ldndc_crop_parent' : ':INDICA:',
        'ldndc_crop_mnemonic' : 'TUBIGAN18',
        'ldndc_crop_name' : 'Rice',
        'GGCMI_crop_short' : 'ri1',
        'GGCMI_crop_fert' : 'ric',
        'Tbase' : 8,
        'Tmax' : 40,
        'vernalisation_flag' : 0,
        'Nsites' : 66621
    },

    'rice2_rf' : {
        'ecosystem_type' : 'arable',
        'ldndc_crop_parent' : ':INDICA:',
        'ldndc_crop_mnemonic' : 'SAHODULAN1',
        'ldndc_crop_name' : 'Rice',
        'GGCMI_crop_short' : 'ri2',
        'GGCMI_crop_fert' : 'ric',
        'Tbase' : 8,
        'Tmax' : 40,
        'vernalisation_flag' : 0,
        'Nsites' : 30696
    },

    'rice2_ir' : {
        'ecosystem_type' : 'arable',
        'ldndc_crop_parent' : ':INDICA:',
        'ldndc_crop_mnemonic' : 'TUBIGAN18',
        'ldndc_crop_name' : 'Rice',
        'GGCMI_crop_short' : 'ri2',
        'GGCMI_crop_fert' : 'ric',
        'Tbase' : 8,
        'Tmax' : 40,
        'vernalisation_flag' : 0,
        'Nsites' : 30696
    },

    'soy' : {
        'ecosystem_type' : 'arable',
        'ldndc_crop_parent' : ':LEGUME:',
        'ldndc_crop_mnemonic' : 'SOYB',
        'ldndc_crop_name' : 'Soybeans',
        'GGCMI_crop_short' : 'soy',
        'GGCMI_crop_fert' : 'soy',
        'Tbase' : 7,
        'Tmax' : 40,
        'vernalisation_flag' : 0,
        'Nsites' : 66621
    }
}

#water source dictionary (ensures can't choose rf and firr by mistake)
water_dict = {
    'ir' : {
        'rf_ir' : 'ir',
        'noirr_firr' : 'firr',
        'irr_long' : 'irrigated'
    },

    'rf' : {
        'rf_ir' : 'rf',
        'noirr_firr' : 'noirr',
        'irr_long' : 'rain-fed'
    },
}

#what to extract from ldndc output into netcdf
postprocessing_dict = {
    'yield' : {
        'location' : 'report-harvest',
        'ldndc_name' : 'DW_grain[kgha-1]',
        'GGCMI_name' : 'yield',
        'GGCMI_long_name' : 'yield',
        'GGCMI_units' : 't ha-1 gs-1 (dry matter)',
        'unitconverter' : 0.001  #converts kg/ha to t/ha
    },

    'agb' : {
        'location' : 'report-harvest',
        'ldndc_name' : 'DW_above_ground[kgha-1]',
        'GGCMI_name' : 'biom',
        'GGCMI_long_name' : 'total above ground biomass',
        'GGCMI_units' : 't ha-1 gs-1 (dry matter)',
        'unitconverter' : 0.001  #converts kg/ha to t/ha
    },

    'CN_ratio' : {
        'location' : 'report-harvest',
        'ldndc_name' : 'CN_grain[ratio]',
        'GGCMI_name' : 'cnyield',
        'GGCMI_long_name' : 'CN ratio of yield',
        'GGCMI_units' : '',
        'unitconverter' : 1  #no conversion necessary
    },

    'planting_day' : {
        'location' : 'ggcmi-seasonal',
        'ldndc_name' : 'planting_day',
        'GGCMI_name' : 'plantday',
        'GGCMI_long_name' : 'planting day',
        'GGCMI_units' : 'day of year',
        'unitconverter' : 1  #no conversion necessary
    },

    'planting_year' : {
        'location' : 'ggcmi-seasonal',
        'ldndc_name' : 'planting_year',
        'GGCMI_name' : 'plantyear',
        'GGCMI_long_name' : 'planting year',
        'GGCMI_units' : 'calendar year',
        'unitconverter' : 1  #no conversion necessary
    },

    'anthesis_day' : { #counted from planting day
        'location' : 'ggcmi-seasonal',
        'ldndc_name' : 'anthesis_day',
        'GGCMI_name' : 'anthday',
        'GGCMI_long_name' : 'days until anthesis',
        'GGCMI_units' : 'days from planting',
        'unitconverter' : 1  #no conversion necessary
    },

    'harvest_day' : { #counted from planting day
        'location' : 'ggcmi-seasonal',
        'ldndc_name' : 'harvest_day',
        'GGCMI_name' : 'matyday',
        'GGCMI_long_name' : 'days until maturity',
        'GGCMI_units' : 'days from planting',
        'unitconverter' : 1  #no conversion necessary
    },

    'irrigation' : {
        'location' : 'ggcmi-seasonal',
        'ldndc_name' : 'irri[mm]',
        'GGCMI_name' : 'pirnreq',
        'GGCMI_long_name' : 'potential net irrigation water requirements',
        'GGCMI_units' : 'kg m-2 gs-1',
        'unitconverter' : 1  #convert mm to kg m-2 gs-1
    },

    'aet' : {
        'location' : 'ggcmi-seasonal',
        'ldndc_name' : 'AET[mm]',
        'GGCMI_name' : 'aet',
        'GGCMI_long_name' : 'actual evapotranspiration',
        'GGCMI_units' : 'kg m-2 gs-1',
        'unitconverter' : 1  #convert mm to kg m-2 gs-1
    },

    'transpiration' : {
        'location' : 'ggcmi-seasonal',
        'ldndc_name' : 'transp[mm]',
        'GGCMI_name' : 'transp',
        'GGCMI_long_name' : 'transpiration',
        'GGCMI_units' : 'kg m-2 gs-1',
        'unitconverter' : 1  #convert mm to kg m-2 gs-1
    },

    'evaporation' : {
        'location' : 'ggcmi-seasonal',
        'ldndc_name' : 'evap[mm]',
        'GGCMI_name' : 'evap',
        'GGCMI_long_name' : 'soil evaporation',
        'GGCMI_units' : 'kg m-2 gs-1',
        'unitconverter' : 1  #convert mm to kg m-2 gs-1
    },

    'runoff' : {
        'location' : 'ggcmi-seasonal',
        'ldndc_name' : 'runoff[mm]',
        'GGCMI_name' : 'runoff',
        'GGCMI_long_name' : 'total runoff',
        'GGCMI_units' : 'kg m-2 gs-1',
        'unitconverter' : 1  #convert mm to kg m-2 gs-1
    },

    'root_biomass' : {
        'location' : 'report-harvest',
        'ldndc_name' : 'DW_roots[kgha-1]',
        'GGCMI_name' : 'rootm',
        'GGCMI_long_name' : 'total root biomass',
        'GGCMI_units' : 't ha-1 gs-1 (dry matter)',
        'unitconverter' : 0.001  #converts kg/ha to t/ha
    },

    'Nuptake' : {
        'location' : 'ggcmi-seasonal',
        'ldndc_name' : 'dN_up[kgNha-1]',
        'GGCMI_name' : 'tnrup',
        'GGCMI_long_name' : 'total uptake of reactive nitrogen',
        'GGCMI_units' : 'kgN ha-1 gs-1',
        'unitconverter' : 1  #no conversion needed
    },

    'Ninput' : { #need to add on fertiliser and manure
        'location' : 'ggcmi-seasonal',
        'ldndc_name' : 'dN_in[kgNha-1]',
        'GGCMI_name' : 'tnrin',
        'GGCMI_long_name' : 'total input of reactive nitrogen including biological fixation',
        'GGCMI_units' : 'kgN ha-1 gs-1',
        'unitconverter' : 1  #no conversion needed
    },

    'fertiliser' : {
        'location' : 'report-fertilize',
        'ldndc_name' : 'dN_fertilizer[kgNha-1]',
        'GGCMI_name' : '-',
        'GGCMI_long_name' : '-',
        'GGCMI_units' : 'kgN ha-1 gs-1',
        'unitconverter' : 1  #no conversion needed
    },

    'manure' : {
        'location' : 'report-manure',
        'ldndc_name' : 'dN_manure[kgNha-1]',
        'GGCMI_name' : '-',
        'GGCMI_long_name' : '-',
        'GGCMI_units' : 'kgN ha-1 gs-1',
        'unitconverter' : 1  #no conversion needed
    },

    'Nloss' : {
        'location' : 'ggcmi-seasonal',
        'ldndc_name' : 'dN_out[kgNha-1]',
        'GGCMI_name' : 'tnrloss',
        'GGCMI_long_name' : 'total losses of reactive nitrogen',
        'GGCMI_units' : 'kgN ha-1 gs-1',
        'unitconverter' : 1  #no conversion needed
    },

    'Ngrain' : {
        'location' : 'report-harvest',
        'ldndc_name' : 'N_grain[kgNha-1]',
        'GGCMI_name' : '',
        'GGCMI_long_name' : '',
        'GGCMI_units' : 'kgN ha-1 gs-1',
        'unitconverter' : 1  #no conversion necessary
    },

    'Nstraw_export' : {
        'location' : 'report-harvest',
        'ldndc_name' : 'N_straw_export[kgNha-1]',
        'GGCMI_name' : '',
        'GGCMI_long_name' : '',
        'GGCMI_units' : 'kgN ha-1 gs-1',
        'unitconverter' : 1  #no conversion necessary
    },

    'N2O' : {
        'location' : 'ggcmi-seasonal',
        'ldndc_name' : 'dN_n2o_emis[kgNha-1]',
        'GGCMI_name' : 'n2oemis',
        'GGCMI_long_name' : 'total N2O emissions',
        'GGCMI_units' : 'gN m-2 gs-1',
        'unitconverter' : 0.1  #converts kgN/ha to gN/m-2
    },

    'N2' : {
        'location' : 'ggcmi-seasonal',
        'ldndc_name' : 'dN_n2_emis[kgNha-1]',
        'GGCMI_name' : 'n2emis',
        'GGCMI_long_name' : 'total N2 emissions',
        'GGCMI_units' : 'gN m-2 gs-1',
        'unitconverter' : 0.1  #converts kgN/ha to gN/m-2
    },

    'Nleach' : {
        'location' : 'ggcmi-seasonal',
        'ldndc_name' : 'dN_leach[kgNha-1]',
        'GGCMI_name' : 'nleach',
        'GGCMI_long_name' : 'total leaching losses of reactive nitrogen',
        'GGCMI_units' : 'gN m-2 gs-1',
        'unitconverter' : 0.1  #converts kgN/ha to gN/m-2
    },

    'Cemis' : {
        'location' : 'ggcmi-seasonal',
        'ldndc_name' : 'dC_emis[kgCha-1]',
        'GGCMI_name' : 'tcemis',
        'GGCMI_long_name' : 'total carbon emissions',
        'GGCMI_units' : 'gC m-2 gs-1',
        'unitconverter' : 0.1  #converts kgNC/ha to gC/m-2
    },

    'CH4emis' : {
        'location' : 'ggcmi-seasonal',
        'ldndc_name' : 'dC_ch4_emis[kgCha-1]',
        'GGCMI_name' : 'ch4emis',
        'GGCMI_long_name' : 'total CH4 emissions',
        'GGCMI_units' : 'gC m-2 gs-1',
        'unitconverter' : 0.1  #converts kgNC/ha to gC/m-2
    },

    'dvs' : {
        'location' : 'ggcmi-seasonal',
        'ldndc_name' : 'dvs',
        'GGCMI_name' : '-',
        'GGCMI_long_name' : '-',
        'GGCMI_units' : '-',
        'unitconverter' : 1
    },

    'soilwater' : {
        'location' : 'ggcmi-monthly',
        'ldndc_name' : 'soilwater[mm]',
        'GGCMI_name' : 'soilmoist1m',
        'GGCMI_long_name' : 'plant available water in upper 1m soil',
        'GGCMI_units' : 'kg m-3',
        'unitconverter' : 1  #converts mm to kg m-3
    }
}


postprocessing_dict_yearly = {

    'N2emis' : {
        'ldndc_name' : 'dN_n2_emis[kgNha-1]',
        'long_name' : 'yearly emissions of N2',
        'units' : 'kgN/ha/yr'
    },

    'NH3emis' : {
        'ldndc_name' : 'dN_nh3_emis[kgNha-1]',
        'long_name' : 'yearly emissions of NH3',
        'units' : 'kgN/ha/yr'
    },

    'N2Oemis' : {
        'ldndc_name' : 'dN_n2o_emis[kgNha-1]',
        'long_name' : 'yearly emissions of N2O',
        'units' : 'kgN/ha/yr'
    },

    'NOemis' : {
        'ldndc_name' : 'dN_no_emis[kgNha-1]',
        'long_name' : 'yearly emissions of NO',
        'units' : 'kgN/ha/yr'
    },

    'NO3leach' : {
        'ldndc_name' : 'dN_no3_leach[kgNha-1]',
        'long_name' : 'yearly leaching of NO3',
        'units' : 'kgN/ha/yr'
    },

    'DONleach' : {
        'ldndc_name' : 'dN_don_leach[kgNha-1]',
        'long_name' : 'yearly leaching of dissolved organic N',
        'units' : 'kgN/ha/yr'
    },

    'NH4leach' : {
        'ldndc_name' : 'dN_nh4_leach[kgNha-1]',
        'long_name' : 'yearly leaching of NH4',
        'units' : 'kgN/ha/yr'
    },

    'Nexp_harvest' : {
        'ldndc_name' : 'dN_harvest_export[kgNha-1]',
        'long_name' : 'yearly export of N via harvest',
        'units' : 'kgN/ha/yr'
    },

    'Ndep' : {
        'ldndc_name' : 'dN_dep[kgNha-1]',
        'long_name' : 'yearly deposition of N',
        'units' : 'kgN/ha/yr'
    },

    'Nfix' : {
        'ldndc_name' : 'dN_fix[kgNha-1]',
        'long_name' : 'yearly fixation of N',
        'units' : 'kgN/ha/yr'
    },

    'Nfert' : {
        'ldndc_name' : 'dN_fert[kgNha-1]',
        'long_name' : 'yearly additiona of N fertiliser',
        'units' : 'kgN/ha/yr'
    },

    'Nup' : {
        'ldndc_name' : 'dN_up[kgNha-1]',
        'long_name' : 'yearly plant uptake of N',
        'units' : 'kgN/ha/yr'
    },

    'Nlit_abv' : {
        'ldndc_name' : 'dN_litter_above[kgNha-1]',
        'long_name' : 'yearly return of N to soil from above-ground plant litter',
        'units' : 'kgN/ha/yr'
    },

    'Nlit_blw' : {
        'ldndc_name' : 'dN_litter_below[kgNha-1]',
        'long_name' : 'yearly return of N to soil from below-ground plant litter',
        'units' : 'kgN/ha/yr'
    },

    'CO2emis_hetero' : {
        'ldndc_name' : 'dC_co2_hetero_emis[kgCha-1]',
        'long_name' : 'yearly CO2 emissions from heterotrophs',
        'units' : 'kgC/ha/yr'
    },

    'CO2emis_auto' : {
        'ldndc_name' : 'dC_co2_auto_emis[kgCha-1]',
        'long_name' : 'yearly CO2 emissions from autotrophs',
        'units' : 'kgC/ha/yr'
    },

    'CH4emis' : {
        'ldndc_name' : 'dC_ch4_emis[kgCha-1]',
        'long_name' : 'yearly CH4 emissions',
        'units' : 'kgC/ha/yr'
    },

    'Cleach' : {
        'ldndc_name' : 'dC_leach[kgCha-1]',
        'long_name' : 'yearly leaching of CH4 and dissolved organic C',
        'units' : 'kgC/ha/yr'
    },

    'Cfert' : {
        'ldndc_name' : 'dC_fert[kgCha-1]',
        'long_name' : 'yearly addition of C via fertiliser',
        'units' : 'kgC/ha/yr'
    },

    'Clit_abv' : {
        'ldndc_name' : 'dC_litter_above[kgCha-1]',
        'long_name' : 'yearly addition of C to soil from above-ground plant litter',
        'units' : 'kgC/ha/yr'
    },

    'Clit_blw' : {
        'ldndc_name' : 'dC_litter_below[kgCha-1]',
        'long_name' : 'yearly addition of C to soil from below-ground plant litter',
        'units' : 'kgC/ha/yr'
    },

    'Cfix' : {
        'ldndc_name' : 'dC_fix[kgCha-1]',
        'long_name' : 'yearly C fixation',
        'units' : 'kgC/ha/yr'
    },

    'throughfall' : {
        'ldndc_name' : 'throughfall[mm]',
        'long_name' : 'yearly throughfall',
        'units' : 'mm'
    },

    'interception' : {
        'ldndc_name' : 'intercep[mm]',
        'long_name' : 'yearly evaporation from interception',
        'units' : 'mm'
    },

    'transpiration' : {
        'ldndc_name' : 'transp[mm]',
        'long_name' : 'yearly transpiration',
        'units' : 'mm'
    },

    'evaporation' : {
        'ldndc_name' : 'evap[mm]',
        'long_name' : 'yearly evaporation from soil and surface water',
        'units' : 'mm'
    },

    'runoff' : {
        'ldndc_name' : 'runoff[mm]',
        'long_name' : 'yearly surface runoff',
        'units' : 'mm'
    },

    'percolation' : {
        'ldndc_name' : 'perc[mm]',
        'long_name' : 'yearly percolation out of bottom soil layer',
        'units' : 'mm'
    },

    'irrigation' : {
        'ldndc_name' : 'irri[mm]',
        'long_name' : 'yearly supply of irrigation water',
        'units' : 'mm'
    }
}

#folders dictionary
folders_dict = {
    'IMK' : { #laptop at work
        'resources_folder' : '/Users/smerald-a/ldndc/resources/GGCMI_phase3/',
        'resources_folder2' : '/Users/smerald-a/ldndc/resources/GGCMI_phase3/',
        'resources_folder_yg' : '/Users/smerald-a/ldndc/resources/yield_gap/',
        'input_folder' : '/Users/smerald-a/ldndc/projects/N_distribution/input_files/',
        'output_folder' : '../site_output/', #path to output folder from input folder
        'output_folder2' : '../site_output/', #path to output folder from location of post_processor.py
        'Lresources_folder' : '/Users/smerald-a/.ldndc/Lresources',
        'relative_resource_path' : ''
    },

    'bio' : {
        'resources_folder' : '/pd/data/bio/datastore.ldndc/GGCMI_phase3_resources/',
        'resources_folder2' : '/pd/data/bio/datastore.ldndc/GGCMI_phase3_resources/',
        'input_folder' : './input_files/',
        'output_folder' : '../output/',  #path to output folder from input folder
        'output_folder2' : './output/', #path to output folder from location of post_processor.py
        'Lresources_folder' : '/pd/home/smerald-a/.ldndc/Lresources',
        'relative_resource_path' : '../../../../../..'
    },

    'kea' : {
        'resources_folder' : '/pd/data/bio/datastore.ldndc/GGCMI_phase3_resources/',
        'resources_folder2' : '/pd/data/bio/datastore.ldndc/GGCMI_phase3_resources/',
        'input_folder' : './input_files/',
        'output_folder' : '',  #path to output folder from input folder
        'output_folder2' : '', #path to output folder from location of post_processor.py
        'Lresources_folder' : '/pd/home/smerald-a/.ldndc/Lresources',
        'relative_resource_path' : ''
    },

    'uc2' : {
        'resources_folder' : '/pfs/work7/workspace/scratch/df4021-GGCMI_phase3_resources-0/',
        'resources_folder2' : '',
        'resources_folder_yg' : '/pfs/work7/workspace/scratch/df4021-GGCMI_phase3_resources-0/',
        'input_folder' : './input_files/',
        'output_folder' : '',  #path to output folder from where job is running
        'output_folder2' : './output/', #path to output folder from location of post_processor.py
        'Lresources_folder' : '/home/kit/imk-ifu/df4021/.ldndc/Lresources',
        'relative_resource_path' : ''
    },
}

queues = {
    'ivy' : {
        'tasks_per_node' : 20,
        'tasks_per_core' : 2
    },

    'cclake' : {
        'tasks_per_node' : 40,
        'tasks_per_core' : 2
    },

    'rome' : {
        'tasks_per_node' : 64,
        'tasks_per_core' : 1
    },

    'dev_single' : {
        'tasks_per_node' : 40,
        'tasks_per_core' : 2
    },

    'single' : { #BWUnicluster single node computing partition
        'tasks_per_node' : 40,
        'tasks_per_core' : 2
    },

    'dev_multiple' : {
        'tasks_per_node' : 40,
        'tasks_per_core' : 2
    },

    'multiple' : { #BWUnicluster parallel computing partition (must ask for 2 or more nodes)
        'tasks_per_node' : 40,
        'tasks_per_core' : 2
    },

    'multiple_e' : { #BWUnicluster Broadwell partition (must ask for 2 or more nodes)
        'tasks_per_node' : 28,
        'tasks_per_core' : 2
    }

}
