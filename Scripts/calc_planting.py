#%% import libraries
import numpy as np
import xarray as xr
from multiprocessing import Pool
import math 
import time 
import sys
 
# local 
import home
import dicts
from helperFunctions import catch_filenames

#import importlib
#importlib.reload(home)

#%% general selection 
#crop
if len(sys.argv) < 2: 
    crop = "maize"
    print("Warning: <cropname> missing, defaulting to maize")
else:
    crop = sys.argv[1]

#scenario
if len(sys.argv) < 3: 
    scenario = 2038
    print("Warning: <scenario> missing, defaulting to 2038")
else:
    scenario = sys.argv[2]
    scenario = int(scenario)

res  = "025"
watering = "rf"

mode = "run" #input('Wich mode? (test/run): ').lower()
parallel = True #input('Run parallel? (Y/N): ').lower() in ['y', 'yes']
#%%
start_script = time.time()
print(f"start script {sys.argv[0]} at {time.strftime('%Y-%m-%d %X',time.localtime())}")
#logging.basicConfig(format='%(levelname)s:%(message)s',filename=f"{home.hspath}/Slurm/planting_{crop}_f{scenario}.out",level = logging.INFO)
#%% Import Climate variables

base_vars = xr.load_dataset(home.climatepath + "/scenic_EU025/scenic_EU025_ssp370_f2017_daily.nc")

scenic_vars = xr.load_dataset(home.climatepath + f"/scenic_EU025/scenic_EU025_ssp370_f{scenario}_daily.nc")
additional_vars = xr.load_dataset(home.climatepath + f"/scenic_EU025/scenic_EU025_ssp370_f{scenario}_daily_extra.nc")

additional_vars = additional_vars.rename({"time":"date"})

#%% merge datasets


clim_vars = xr.merge([scenic_vars,additional_vars])

clim_vars["doy"] = clim_vars.date.dt.dayofyear


#clim_vars["date2"] = clim_vars.date
#clim_vars = clim_vars.set_index(date = "doy")
#%% Import Crop Calendar 
ggcmi_crop = dicts.crop_dict[crop]["GGCMI_crop_short"]

cc_crop = xr.open_dataset(f"{home.homepath}/scenic_heatstress/crop_calendar/GGCMI_crop_calendars_EU{res}/{ggcmi_crop}_{watering}_ggcmi_crop_calendar_EU{res}.nc4")

#%% get average temperatures for 2017 - 2022 for planting 

tavg_planting = base_vars.tavg.where(base_vars.date.dt.dayofyear == cc_crop.planting_day,drop=True).mean(dim="date")
tstd_planting = base_vars.tavg.where(base_vars.date.dt.dayofyear == cc_crop.planting_day,drop=True).std(dim="date")
tavg_planting.attrs = {"standard_name":"air_temperature","units":"°C","long_name":"average temperature on the planting day as provided by the GGCMI Crop Calendars"}

#tstd_planting.plot()
#tavg_planting.to_dataset(name="tavg_planting").to_netcdf(home.cc_path + "/tavg_planting_2017_to_2022.nc")


#%% wrap in function 

def adjust_planting(ilat,ilon,year_vals):
    print(f"***************************{ilat},{ilon}***************************")
    list_year = []
    for year in year_vals:
        print(f"==========={year}===========")
        cc_cell = cc_crop.sel(lat = ilat,lon = ilon)
        clim_cell = clim_vars.sel(lat = ilat,lon = ilon).where(clim_vars.date.dt.year == year,drop=True)
        tp_base_cell = tavg_planting.sel(lat = ilat,lon = ilon).values
        #tp_std_cell = tstd_planting.sel(lat = ilat,lon = ilon).values
        p_cc_cell = cc_cell.planting_day.values

        if (np.any([np.isnan(p_cc_cell),np.isnan(tp_base_cell)])):
            p_cell = np.nan 
    # calculate if tavg on on crop sowing date from GGCMI crop calendar is higher than the average for that gridcell
        else: 
            print("Planting day in cell: ",p_cc_cell)
            print("Average T at planting:",tp_base_cell)
            # get the tem
            # initialize the cell planting date with the date from the crop calendar 
            p_cell = p_cc_cell 
            searching = True 
            while(searching & ( p_cell > 80)): 
                t_dc = clim_cell.where(clim_cell.doy == p_cell,drop=True).tavg.values
                # get conditions for that day
                no_snow = (clim_cell.where(clim_cell.doy==p_cell,drop=True).fr_snow.values) == 0
                no_soil_frost = np.all((clim_cell.where(clim_cell.doy==p_cell,drop=True).sel(depth=slice(5,20)).t_so.values) > 0)
                # check whether there was any precipitation event >10 mm/d in the last week (Iizumi 2019)
                rain = clim_cell.where((clim_cell.doy > p_cell - 8) & (clim_cell.doy < p_cell),drop=True).prec.max().values > 10
                mud = np.any(clim_cell.where(clim_cell.doy == p_cell,drop=True).smi.sel(depth=slice(5,20)).values > 1)
                #mud_last_week = clim_cell.where((clim_cell.doy > p_cell - 8) & (clim_cell.doy < p_cell),drop=True).smi.sel(depth=slice(5,20)).mean(dim="depth") > 1
                #first_doy_without_mud = clim_cell.where((clim_cell.doy > p_cell - 8) & (clim_cell.doy < p_cell),drop=True).doy[~mud_last_week][-1]
                #if (np.any(~mud_last_week).values): 
                #    first_doy_without_mud = mud_last_week.where(~mud_last_week,drop=True).date.dt.dayofyear.min().values
                if ((t_dc > tp_base_cell) & (p_cell <= p_cc_cell)):
                    if (no_snow):
                        #no snow
                        if (no_soil_frost):
                            #no frozen soil 
                            if (rain):
                                #check wheater the rain has caused muddyness (True = yes, mud)
                                if(mud):
                                    print(p_cell,": precip > 10mm/d in the last week & soil too moist, not drivable")
                                    print("SMI 5cm:",clim_cell.where(clim_cell.doy == p_cell,drop=True).smi.sel(depth=5).values,
                                          "\nSMI 20cm:",clim_cell.where(clim_cell.doy == p_cell,drop=True).smi.sel(depth=20).values)
                                    #if soil is not drivable, move planting one day along 
                                    p_cell = p_cell + 1
                                    #t_dc = clim_cell.where(clim_cell.doy == p_cell,drop=True).tavg.values
                                    searching = False

                                    #print("not moving planting day")
                                elif (~mud):
                                    print(p_cell,": precip > 10mm/d in the last week, but soil dry enough to plant")                            
                                    #t_dc = clim_cell.where(clim_cell.doy == p_cell,drop=True).tavg.values
                                    p_cell = p_cell - 1 
                                    #p_cell = first_doy_without_mud.values
                                    #print(p_cell,"planting day set to first day without mud")

                                    #print("moving planting day -1")
                            elif(no_snow & no_soil_frost & ~rain):
                                print(p_cell,": no snow, no frost, no large precipitation > 10 mm/d in the last 7 days:",round(*t_dc,2))
                                p_cell = p_cell - 1 
                                #t_dc = clim_cell.where(clim_cell.doy == p_cell,drop=True).tavg.values
                                #print("moving planting day -1")
                        elif(~no_soil_frost):
                            print(p_cell,"soil frost")
                            print(t_dc)
                            p_cell = p_cell + 1
                            searching = False
                            #t_dc = clim_cell.where(clim_cell.doy == p_cell,drop=True).tavg.values
                            #finish_loop(p_cell)
                    elif(~no_snow): 
                        print(p_cell,": snow")
                        p_cell = p_cell + 1
                        searching = False
                        #t_dc = clim_cell.where(clim_cell.doy == p_cell,drop=True).tavg.values
                        #finish_loop(p_cell)
                elif(((t_dc < tp_base_cell) | ~no_snow | ~no_soil_frost |(rain & mud))  & (p_cell < p_cc_cell)):
                    if (t_dc < tp_base_cell):
                        print(p_cell,": colder than tp_base_cell:",round(*t_dc,2))
                    if(~no_snow):
                        print("snow")
                    if(~no_soil_frost):
                        print("soil frost")
                    if((rain & mud)):
                        print("muddy from rainfall")
                    p_cell = p_cell + 1
                    print("stop finding new date, since another day was already favorable")
                    searching = False
                elif(((t_dc < tp_base_cell) | ~no_snow | ~no_soil_frost |(rain & mud))  & (p_cell >= p_cc_cell)):
                    if (t_dc < tp_base_cell):
                        print(p_cell,": colder than tp_base_cell:",round(*t_dc,2))
                    if(~no_snow):
                        print("\t\tsnow")
                    if(~no_soil_frost):
                        print("\t\tsoil frost")
                    if((rain & mud)):
                        print("\t\tmuddy from rainfall")
                    p_cell = p_cell + 1
                elif((t_dc > tp_base_cell) & (p_cell > p_cc_cell)):
                    print(p_cell,": conditions now favorable")
                    print(p_cell,": no snow, no frost, no large precipitation > 10 mm/d in the last 7 days")                            
                    searching = False
                else:
                    print("aborting due to to no condition met")
                    searching = False
            else:     
                t_dc = clim_cell.where(clim_cell.doy == p_cell,drop=True).tavg.values
                p_diff = p_cell - p_cc_cell
                print(f"{ilat},{ilon},{year}: New day of planting:",p_cell,", moved plating by",math.copysign(p_diff,p_diff),"days")

                print("temp at",p_cell,":",round(*t_dc,2))
                searching = False    
        list_year.append(p_cell)
    return(list_year)
#%%
if mode == "test":
    if (res == "025"):
        #lat_vals = [50.125, 50.375, 50.625, 50.875]
        #lon_vals = [8.125, 8.375, 8.625, 8.875]
        lat_vals = [36.375,36.625]
        lon_vals = [-3.375,-3.125,-2.625]
    if (res == "011"):
        lat_vals = [40.485 , 40.5961, 40.7072, 40.8183, 40.9294]#
        lon_vals = [23.07  , 23.1813, 23.2926, 23.4039, 23.5152]
else: 
    lat_vals = clim_vars.lat.values
    lon_vals = clim_vars.lon.values

year_vals = np.unique(clim_vars.date.dt.year)

    
# #%% non -parallel
# start = time.time()
# planting_array = []
# planting_lons = []
# for lat in lat_vals:
#     for lon in lon_vals:
#         print(f"***************************{lat},{lon}***************************")
#         cell = adjust_planting(lat,lon,year_vals)
#         print(cell)
#         planting_lons.append(cell)
#     planting_array.append(planting_lons)
#     planting_lons = []
# end = time.time()

# time_unparallel = end - start
# print(f"Script successfull. Ran for {round(time_unparallel/60,2)} minutes")

# #%% parallel take 2 (loop for year within function)
# start = time.time()
# planting_array = []
# var_list = []
# ncores = 10

# with Pool(ncores) as p:
#     for lat in lat_vals:
#         for lon in lon_vals:
#             var_list.append((lat,lon,year_vals))
#         cell = p.starmap(adjust_planting,var_list)
#         planting_array.append(cell)
#         var_list = []
# p.close()
# p.join()
      
# end = time.time()
# time_parallel = end - start
# print(f"Script successfull. Ran for {round(time_parallel/60,2)} minutes")

#%% if loop for parallel or non paralllel 


if parallel:
     start = time.time()
     planting_array = []
     var_list = []
     ncores = 20
     print(f"running on {ncores} cores")
     with Pool(ncores) as p:
         for lat in lat_vals:
             for lon in lon_vals:
                 var_list.append((lat,lon,year_vals))
             cell = p.starmap(adjust_planting,var_list)
             planting_array.append(cell)
             var_list = []
     p.close()
     p.join()
  
else:
    planting_array = []
    planting_lons = []
    for lat in lat_vals:
        for lon in lon_vals:
            cell = adjust_planting(lat,lon,year_vals)
            planting_lons.append(cell)
        planting_array.append(planting_lons)
        planting_lons = []
     
#%%  create empty array 
lat_array = xr.DataArray(lat_vals,dims="lat", name = "lat")
lon_array = xr.DataArray(lon_vals,dims="lon", name = "lon")
time_array = xr.DataArray(year_vals,dims = "year",name = "year")
coords = {"lat":lat_array,"lon":lon_array,"year": time_array}
# pass nested list to array 
da = xr.DataArray(planting_array,dims=["lat","lon","year"], 
                          coords=coords)


#%%save
print("..finished calculations, start saving...")
da = da.to_dataset(name="planting_day")
da.attrs={"forcing":f"f{scenario}",
     "grid resolution":int(res)/100,
     "creation date":time.strftime('%Y-%m-%d %H:%M:%S',time.localtime()),
     "script":sys.argv[0],
     "contact":home.email}
da.lat.attrs={'standard_name': 'latitude',
 'long_name': 'latitude',
 'units': 'degrees_north',
 'axis': 'Y'}
da.lon.attrs={'standard_name': 'longitude',
 'long_name': 'longitude',
 'units': 'degrees_east',
 'axis': 'X'}
da.year.attrs={'standard_name': 'year', 'axis': 'T'}
da.planting_day.attrs={"planting_day":"calculated date at which planting is first possible, based on climate",
     "crop":crop,
     "watering method":watering
     }

date = time.strftime('%Y-%m-%d',time.localtime())
filename_da = f"{home.hspath}/crop_calendar/planting_date_calculation/dynamic_planting_{crop}_f{scenario}_{date}"
filename_da = catch_filenames(filename_da,mode)

da.to_netcdf(f"{filename_da}.nc")
#da.isel(year=2).planting_day.plot()
print(f"saved file {filename_da}.nc")
end_script = time.time() 
time_script = end_script - start_script
print(f"Script successfull. Ran for {round(time_script,2)/60} minutes")
#%% parallel 
# if parallel:
#       logging.info("running parallel")
#      # hrgf_lon = []
#      hrgf_latlon = []
#      lon_list = []
#      ncores = 40
#      logging.info("running on %s cores", ncores)
#      with Pool(ncores) as p:
#          # create nested lat lon year list
#          for lat in lat_vals:
#             for lon in lon_vals:
#                 logging.info(f"{lat},{lon}")
#                 lon_list.append((lat, lon, t_crit_min))
#             hrgf_lon = p.starmap(reduction_grainfilling, lon_list)
#             hrgf_latlon.append(hrgf_lon)
#             lon_list = []  # reset
        
# logging.info(f"loop of {ncores} finalized")
# p.close()
# p.join()


#%% run indiviudal cells 

# gridcell = ( 59.125, 11.125) # upper bavaria north of munich
# #gridcell = (53.125,24.125) # Belarus
# #gridcell = (58.125,25.125) # Estonia
# #gridcell = (64.125,24.125) #  Finnland
# #gridcell = ( 34.875, -10.375)  #Problem Cell 
# year = 2038
# ilat=gridcell[0]
# ilon=gridcell[1]

# adjust_planting(ilat,ilon,year_vals)

# #%%
# year_vals = np.unique(clim_vars.date.dt.year)

# #%%
# planting_lons = []
# planting_year = []
# lat=gridcell[0]
# lon=gridcell[1]



# for year in year_vals:
#     logging.info("-------------------",year,"------------------")
#     p_cell = adjust_planting(lat,lon,year,cc_crop,clim_vars)
#     logging.info("final planting day in",year,":",p_cell)
#     planting_year.append(p_cell)
# logging.info(planting_year)
# #
# cc_cell = cc_crop.sel(lat = gridcell[0],lon = gridcell[1])
# clim_cell = clim_vars.sel(lat = gridcell[0],lon = gridcell[1]).where(clim_vars.date.dt.year == year,drop=True)
# tp_base_cell = tavg_planting.sel(lat = gridcell[0],lon = gridcell[1]).values
# p_cc_cell = cc_cell.planting_day.values


#%% develop function for one gridcell
# t_dc = clim_cell.where(clim_cell.doy == p_cc_cell,drop=True).tavg.values
# t_wc = clim_cell.where((clim_cell.doy > (p_cc_cell - 7)) & (clim_cell.doy < p_cc_cell),drop=True).tavg.mean(dim="date").values

#%% calculate if tavg on on crop sowing date from GGCMI crop calendar is higher than Tbase 
# t_dc = clim_cell.where(clim_cell.doy == p_cc_cell,drop=True).tavg.values

# if (t_dc > dicts.crop_dict[crop]["Tbase"]):
#     logging.info(t_dc)
#     p_cc_cell = p_cc_cell - 1 
#     t_dc = clim_cell.where(clim_cell.doy == p_cc_cell,drop=True).tavg.values
# else: 
#     logging.info("colder than Tbase")
    
# logging.info(p_cc_cell)

#%% visually analyze soil moisture index (SMI)

# clim_cell.where(clim_cell.doy==p_cc_cell,drop=True).smi
# clim_vars.smi.max();clim_vars.smi.min()
# clim_cell.smi.plot(x = "date",y = "depth")

# clim_cell.smi.where((clim_cell.doy>100) & (clim_cell.doy < 160)).plot(hue = "depth")


# #%%
# clim_cell_week = clim_cell.resample(date = "7d").mean()

# p_earliest_cell = clim_cell_week.doy.where(clim_cell_week.tavg > tp_base_cell,drop=True)[0]

# clim_cell_week.where(clim_cell_week.doy == p_earliest_cell,drop = True)


# import matplotlib.pyplot as plt
# clim_cell.tavg.where((clim_cell.doy>100) & (clim_cell.doy < 160)).plot()
# clim_cell.smi.where((clim_cell.doy>100) & (clim_cell.doy < 160)).plot(hue = "depth")
# plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
# plt.axvline(clim_cell.date.where(clim_cell.doy == p_cc_cell,drop=True).date.values[0],color="r")
# plt.axvline(clim_cell.date.where(clim_cell.doy == p_cell,drop=True).date.values[0],color="b")
# plt.axvline(clim_cell.date.where(clim_cell.doy == p_earliest_cell,drop=True).date.values[0],color="g")

# clim_vars.where(clim_vars.doy == cc_crop.planting_day,drop=True).mean(dim="date").smi.sel(depth = 5).plot(cmap="Set2")

#%% visizalize output
plotting = False

if plotting:
    print("Plotting activated")
    import matplotlib.pyplot as plt

    da = xr.load_dataset(filename_da,+".nc")
    year_vals = np.unique(da.planting_day.year.values)
    for y in year_vals:
        plotname = filename_da.removeprefix(f"{home.hspath}/crop_calendar/planting_date_calculation/")
        da.planting_day.sel(year=y).plot()
        plt.savefig(f"{home.hspath}/Plots/dynamic_planting/{plotname}_{y}.png",dpi=300,bbox_inches ="tight")
        plt.show()
        print(f"saved {home.hspath}/Plots/dynamic_planting/{plotname}_{y}.png")