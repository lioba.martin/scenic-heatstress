## heatstress
import numpy as np
import pandas as pd
import xarray as xr
from multiprocessing import Pool
import logging
import math 
import sys
import time
import glob
import os
import argparse
## local modules 
import dicts 
import home 
import helperFunctions as hf 

#%%
def timing_heatshock(temperature_vector,t_crit_min):
    start_heat = np.nan
    end_heat = np.nan
    timing_heatshock = []
    if temperature_vector.max() < t_crit_min:
        timing_heatshock = []
    else:
        while len(timing_heatshock) == 0: 
            for i in range(0,len(temperature_vector)): 
                if ((i == 0) & (temperature_vector[i] >= t_crit_min)):
                    start_heat = i
                elif ((i == 0) & (temperature_vector[i] < t_crit_min)):
                    pass
                elif (temperature_vector[i-1] < t_crit_min) & (temperature_vector[i] >= t_crit_min): # & (temperature_vector[i+1] >= t_crit_min)
                    start_heat = i
                if ((i == len(temperature_vector)-1) & (temperature_vector[i] >= t_crit_min)):
                    end_heat = i
                elif ((i == len(temperature_vector)-1) & (temperature_vector[i] < t_crit_min)):
                    pass
                elif ((temperature_vector[i] >= t_crit_min) & (temperature_vector[i+1] < t_crit_min)):
                    end_heat = i
                if ((start_heat >= 0) & (end_heat >= 0 )):
                    timing_heatshock = np.append(timing_heatshock,(end_heat + start_heat)/2 )
                    # once a period has been recorded, reset 
                    start_heat = np.nan
                    end_heat = np.nan
            timing_heatshock = np.unique(timing_heatshock) - 6
    return(timing_heatshock)

#%%
def duration_heatshock(temperature_vector,t_crit_min):
    duration_period = 0
    durations = []

    for t in range(len(temperature_vector)):
        if (temperature_vector[t] > t_crit_min):
            duration_period += 1
        if ((t == 18) & (temperature_vector[t] >= t_crit_min)):   
            durations.append(duration_period)
        elif ((t == 18) & (temperature_vector[t] < t_crit_min)): 
            pass
        elif ((temperature_vector[t] >= t_crit_min) & (temperature_vector[t+1] < t_crit_min)):
            durations.append(duration_period)
            duration_period = 0 #reset
    return(durations)

#%%
def heatstress_parameters(timing,duration,t_crit_min,sensitivity_tcrit = 0.3, sensitivity_tzero = 2.5, intersect_t = 52):
    if (len(timing) == 0)|(len(duration)==0):
        return(pd.DataFrame(columns=["timing","duration","tcrit","tzero"]))
    elif (len(timing) == len(duration)):
        hs_periods = len(timing)
        parameters = np.zeros((len(timing),4))
        for p in range(hs_periods):
                if timing[p] <= 0: 
                    t_crit = np.array([36 + sensitivity_tcrit*(timing[p] - 6),t_crit_min]).min()
                    t_zero = 60 + sensitivity_tzero*(timing[p] - 6)
                elif timing[p] > 0: 
                    t_crit = np.array([37.8 + 1.8*timing[p]  - 3*duration[p],t_crit_min]).min()
                    t_zero = intersect_t + 0.75*timing[p] - 1.5*duration[p]
                #print("timing:",timing[p],"; duration:",duration[p],"day(s);\ncritical Temperature:", t_crit,"°C; \nTemperature at zero pod-set:",t_zero,"°C")
                parameters[p] = np.array([timing[p],duration[p],t_crit,t_zero])
        parameters = pd.DataFrame(parameters,columns=["timing","duration","tcrit","tzero"])#.set_index("timing")
        return(parameters)

        raise ValueError(f"timing and duration must have the same size, timing is len(",len(timing),"), duration is len(",len(duration),")")

#%%
def flowerOpeningFraction(day_of_flowering,days_after_flowering = np.nan,date = np.nan):
    if (np.isnan(days_after_flowering)) & (np.isnan(date)):
        logging.error("error: either float of days_after_flowering or date necessary")
    elif (~np.isnan(days_after_flowering)):
         daf = days_after_flowering
    elif (~np.isnan(date)):
         daf =  (date - day_of_flowering)/ np.timedelta64(1, 'D')
    if ((daf > 0) & (daf <= 12)): 
        openFlowers = 1 / (1 + (1 / 0.015 - 1) * math.exp(-1.4 * daf)) #open flowers today 
        openFlowers_y = 1 / (1 + (1 / 0.015 - 1) * math.exp(-1.4 * (daf - 1)))  # open Flowers yesterday
        frac_flower = openFlowers - openFlowers_y
    else: frac_flower = 0
    return frac_flower

#%%
def heatstress_factor(df_par):
	heatstress = []
	for i in range(len(df_par)):
		line_par = df_par.iloc[i]
		hs = 1 - ((line_par.tmax - line_par.tcrit) / (line_par.tzero - line_par.tcrit)) * line_par.flower_frac
		heatstress.append(hs)
	highest_hs = np.array(heatstress).min()
	return(highest_hs)

#%%
def reduction_grainfilling(lat,lon,t_crit_min):
	#try:
	GDD_slice = GDD_ds.sel(lat=lat,lon=lon)
	if (GDD_slice[f"aGDD"].max().values > 0):
		flowering_slice = flowering_da.sel(lat=lat,lon=lon)
		hrgf_cell = []
		for year in flowering_slice.date.values:
			logging.debug("====================================================")
			logging.debug(f"Year: {year}")
			GDD_yslice = GDD_slice.where(GDD_slice.date.dt.year == year,drop=True)
			GDD_crop = GDD_yslice[f"aGDD"]
			flowering_gdd = flowering_slice.sel(date=year).flowering.values
			if ((GDD_crop.max().values > 0) & (~np.isnan(flowering_gdd)) & (flowering_gdd < GDD_crop.max().values) & (flowering_gdd > flowering_default/4)): 
				flowering_date = GDD_crop.where(GDD_crop >= flowering_gdd,drop=True)[0].date.values.astype('datetime64[D]')
				logging.debug("Flowering Date = %s" ,flowering_date)
				date_sensitivity_start = flowering_date - np.timedelta64(6,"D")
				date_sensitivity_end = flowering_date + np.timedelta64(13,"D")
				logging.debug("Sensitive period from %s until %s",date_sensitivity_start,date_sensitivity_end)

				date_sensitivity_range = np.arange(date_sensitivity_start, date_sensitivity_end, dtype='datetime64[D]')
				if (np.all(np.isin(date_sensitivity_range,GDD_yslice.date.values))):
					tmax_sensitivity_range = GDD_slice.tmax[GDD_slice.date.isin(date_sensitivity_range)].values
					sensitivity_range = pd.DataFrame({"tmax":tmax_sensitivity_range,"timing_rel" :range(-6,13),"timing":range(0,19)},index= date_sensitivity_range)
					sensitivity_range.index.name = "date"
					sensitivity_range = sensitivity_range.reset_index().set_index("timing_rel")
					timing = timing_heatshock(tmax_sensitivity_range,t_crit_min)
					duration = duration_heatshock(tmax_sensitivity_range,t_crit_min)
					param_list = heatstress_parameters(timing,duration,t_crit_min)
					#continue only if there were heatstress events 
					if(param_list.empty): logging.debug("no event"); heat_reduction_grainfilling = 1
					else: 
						sensitivity_range = sensitivity_range.assign(tcrit = np.nan,tzero=np.nan,flower_frac=np.nan)
						for t in range(len(param_list)):
							start = param_list.iloc[t].timing - ((param_list.iloc[t].duration-1)/2)
							end = param_list.iloc[t].timing + ((param_list.iloc[t].duration-1)/2)
							if start == end:
								end = end + 1
							sensitivity_range.loc[min([start,end]):max([start,end+1]),"tcrit"] = param_list.iloc[t].tcrit
							sensitivity_range.loc[min([start,end]):max([start,end+1]),"tzero"] = param_list.iloc[t].tzero
						sensitivity_range = sensitivity_range.reset_index().dropna(subset="tcrit")
						frac = []
						for d in range(len(sensitivity_range)):
							frac_d = flowerOpeningFraction(flowering_date,days_after_flowering=sensitivity_range.iloc[d].timing_rel)
							frac.append(frac_d)
						sensitivity_range.flower_frac = frac

						heat_reduction_grainfilling = heatstress_factor(sensitivity_range)
						logging.debug("Grainfilling reduction due to heat during flowering: %s", heat_reduction_grainfilling)
				else:
					logging.warning("dates out of range")
					heat_reduction_grainfilling = np.nan
			else:
				logging.debug(f"probably too cold: flowering gdd {flowering_gdd.round(2)}, gdd_max: {GDD_crop.max().values}, flowering default: {flowering_default}")
				heat_reduction_grainfilling = 0 
			hrgf_cell.append(heat_reduction_grainfilling)
	else:
		logging.debug("empty cell (probably ocean)")
		hrgf_cell = np.zeros(len(flowering_da.date.values))
	return(hrgf_cell)
	#except:
	#	logging.error(f"Error in Lat {lat}, Lon {lon}")

#%%
if __name__ == "__main__": 
	mode = "run"
	logging.getLogger().addHandler(logging.StreamHandler(sys.stdout))

	parser = argparse.ArgumentParser(description="tcrit arguments",
									 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument("-t", "--tcrit", type=int, help="variable tcrit")
	parser.add_argument("-c", "--crop", help="crop type (maize / winter_wheat / spring_wheat)")
	parser.add_argument("-s", "--scenario",help="scenario (2017 / 2038/ 2065 / 2093)")
	parser.add_argument("-m", "--member", help = "Enemble member (E1 / E2 / E3 / E4 / E5)")
	parser.add_argument("-r", "--resolution", help="resolution of data (0.11°/0.25°)")
	parser.add_argument("-w", "--watering", help="watering method")
	args = parser.parse_args()
	
	parallel = True
	
	dynamic_planting = False
	startup = time.time()
	logging.info(f"start script {sys.argv[0]} at {time.strftime('%Y-%m-%d %X',time.localtime())}")
	#crop
	if args.crop is None: 
		crop = "winter_wheat"
		logging.warning(f"Warning: <cropname> missing, defaulting to winter_wheat")
	else:
		crop = args.crop

	#scenario
	if args.scenario is None:
		scenario = 2017
		logging.warning(f"Error: <scenario> missing, defaulting to 2017")
	else:
		scenario = args.scenario
	scenario = int(scenario)
	print(f"scenario used: {scenario}")
	
	# Ensemble Member
	if args.member is None:
		member = "E1"
		print("Warning: <member> missing, defaulting to E1")
	else:
		member = args.member
		
	#resolution 
	if args.resolution is None: 
		res = "025"
		logging.warning(f"No resolution specified, defaulting to 0.25°")
	else:
		res = args.resolution
		logging.info(f"resolution specified {res}° x 10^(-2)")

	#watering
	if args.watering is None: 
		watering = "rf"
		logging.warning(f"No watering method specified , defaulting to rainfed")
	else:
		watering = args.watering
		logging.info(f"watering method specified {watering}")

	#logging.basicConfig(format='%(levelname)s:%(message)s',filename=f"{home.hshome}/Slurm/HI_{crop}_{scenario}_{res}_{mode}.out",level = logging.INFO)
	ggcmi_crop = dicts.crop_dict[crop]['GGCMI_crop_short']
	
	#tcritmin
	if args.tcrit is None:
		t_crit_min =  dicts.crop_dict[crop]["t_crit_min"]
		tcrit_testing = False
	else:
		t_crit_min = args.tcrit
		tcrit_testing = True
		logging.info(f"uncertantiy testing with tcrit")
	
	flowering_default = dicts.crop_dict[crop]["gdd_flowering"]
	
	logging.info(f"calculating heatstress for {crop} for scenario f{scenario}")
	logging.info(f"t_crit_min: {t_crit_min}")
	
	if (dynamic_planting):
		GDD_file = glob.glob(f"{home.hsproc}/dynamic_planting/{member}_scenic_EU{res}_f{scenario}_GDD_{crop}_{watering}_dynamic*.nc")
	else:
		GDD_file = glob.glob(f"{home.hsproc}/ggcmi_planting/{member}_scenic_EU{res}_f{scenario}_GDD_{crop}_{watering}*.nc")
	flowering_file = glob.glob(f"{home.hsproc}/{member}_scenic_EU{res}_f2017_flowering_{crop}_{watering}*.nc")

	if len(GDD_file) >= 1:
		logging.info(f"using file {sorted(GDD_file,key=os.path.getmtime)[-1]} for GDD") 
	else:
		logging.warning(f"no file '{home.hsproc}/{member}_scenic_EU{res}_f{scenario}_GDD_{crop}_{watering}*.nc'")

	if len(flowering_file) >= 1: 
		logging.info(f"using file {sorted(flowering_file,key=os.path.getmtime)[-1]} for flowering values")
	else: 
		logging.warning(f"no file '{home.hsproc}/{member}_scenic_EU{res}_f2017_flowering_{crop}_{watering}*.nc'")
	GDD_ds = xr.open_dataset(sorted(GDD_file, key=os.path.getmtime)[-1])
	if len(np.unique(GDD_ds.date.dt.year)) < 6:
	    raise ValueError("GDD years < 6")
	flowering_da = xr.open_dataset(sorted(flowering_file, key=os.path.getmtime)[-1])
	logging.info("flowering years as read in: %s",flowering_da.date.values)
	if (len(np.unique(GDD_ds.date.dt.year.values)) < len(flowering_da.date.values)):
		flowering_da = flowering_da.sel(date = flowering_da.date.values[1:])
		logging.debug("GDD and flowering do not the same lenght, removing one spinup year")
		logging.debug("not the same length: GDD years: %s ",np.unique(GDD_ds.date.dt.year.values))
		logging.debug("flowering years after shortening: %s", flowering_da.date.values)
	# add the date difference to use the same flowering date 
	td = GDD_ds.date[0].dt.year.values - flowering_da.date[0].values
	flowering_da["date"] = flowering_da.date + td
	logging.info("flowering years after correction: %s", flowering_da.date.values)
	if (np.any(GDD_ds.lat.values == flowering_da.lat.values) & np.any(GDD_ds.lon.values == flowering_da.lon.values)):
		if mode == "test":
			if (res == "025"):
				lat_vals = [50.125, 50.375, 50.625, 50.875]
				lon_vals = [8.125, 8.375, 8.625, 8.875]
			if (res == "011"):
				lat_vals = [40.485 , 40.5961, 40.7072, 40.8183, 40.9294]
				lon_vals = [23.07  , 23.1813, 23.2926, 23.4039, 23.5152]
		else: 
			lat_vals = GDD_ds.lat.values
			lon_vals = GDD_ds.lon.values
		cells  = len(lat_vals)*len(lon_vals)
		logging.debug(" %s Cells",cells) 
		logging.debug("****************************************************************************")
		if parallel:
			logging.debug("running parallel") 
			#hrgf_lon = []
			hrgf_latlon = []
			lon_list = []
			ncores = 100
			logging.info("running on %s cores",ncores)
			with Pool(ncores) as p:
				# create nested lat lon year list 
				for lat in lat_vals:
					for lon in lon_vals:
						logging.info(f"{lat},{lon}")
						lon_list.append((lat,lon,t_crit_min))
					hrgf_lon = p.starmap(reduction_grainfilling,lon_list)
					hrgf_latlon.append(hrgf_lon)
					lon_list = [] #reset 
				logging.debug("****************************************************************************")
			logging.info(f"loop of {ncores} finalized")
			p.close()
			p.join()
		else:
			logging.info("running non-parallel")
			hrgf_lon = []
			hrgf_latlon = []
			for lat in lat_vals:
				for lon in lon_vals:
					logging.info(f"{lat},{lon}")
					hrgf = reduction_grainfilling(lat,lon,t_crit_min)
				# create nested lat lon year list 
					hrgf_lon.append(hrgf)
					logging.debug("****************************************************************************")
				hrgf_latlon.append(hrgf_lon)
				hrgf_lon = [] #reset 
			logging.debug("****************************************************************************")
		#create empty array 
		lat_array = xr.DataArray(lat_vals,dims="lat", name = "lat")
		lon_array = xr.DataArray(lon_vals,dims="lon", name = "lon")
		time_array = xr.DataArray(flowering_da.date.values,dims = "year",name = "year")
		coords = {"lat":lat_array,"lon":lon_array,"year": time_array}
		# pass nested list to array 
		HI = xr.DataArray(hrgf_latlon,
						  dims=["lat","lon","year"], 
						  coords=coords)
		HI = HI.to_dataset(name="HI")
		HI.attrs={"forcing":f"f{scenario}",
			 "grid resolution":int(res)/100,
			 "creation date":time.strftime('%Y-%m-%d %H:%M:%S',time.localtime()),
			 "script":sys.argv[0],
			 "contact":home.email}
		HI.lat.attrs={'standard_name': 'latitude',
		 'long_name': 'latitude',
		 'units': 'degrees_north',
		 'axis': 'Y'}
		HI.lon.attrs={'standard_name': 'longitude',
		 'long_name': 'longitude',
		 'units': 'degrees_east',
		 'axis': 'X'}
		HI.year.attrs={'standard_name': 'year', 'axis': 'T'}
		HI.HI.attrs={"t_crit_min":t_crit_min,
			 "crop":crop,
			 "watering method":watering
			 }
		date = time.strftime('%Y-%m-%d',time.localtime())
		if tcrit_testing:
			netcdf_name = hf.catch_filenames(f"{home.hsout}/tcrit/{member}_scenic_EU{res}_f{scenario}_HI_{crop}_{watering}_{t_crit_min}",mode) + ".nc"
		elif dynamic_planting:
			netcdf_name = hf.catch_filenames(f"{home.hsout}/dynamic_planting/{member}_scenic_EU{res}_f{scenario}_HI_{crop}_{watering}_dynamic",mode) + ".nc"
		else:
			netcdf_name = hf.catch_filenames(f"{home.hsout}/ggcmi_planting/{member}_scenic_EU{res}_f{scenario}_HI_{crop}_{watering}",mode) + ".nc"
		HI.to_netcdf(netcdf_name,mode="w")
		logging.info("saved %s",netcdf_name)
		endtime = time.time()
		logging.info(f"Script successfull. Ran for {round(endtime-startup,2)/60} minutes")
		print("Script succesfull")
	else: 
		raise ValueError(f"GDD_ds and flowering_da must have identical dimensions")
