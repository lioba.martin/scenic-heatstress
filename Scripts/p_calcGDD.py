# -*- coding: utf-8 -*-
"""
Created on Thu Jun  15 
@author: martin-l
"""

import xarray as xr
import numpy as np
import pandas as pd
from tqdm import tqdm
import datetime
import time
import sys 
import os 
from multiprocessing import Pool
import argparse
#from pathlib import Path
pd.set_option('mode.chained_assignment',None)

#local modules 

import home
import dicts
import helperFunctions as hf

#%% general settings 
mode = "run"#input('Wich mode? (test/run): ').lower()
if (mode not in ["run","test"]):
    raise ValueError(f"mode: {mode}, must be either 'run' or 'test'")	
planting_mode = "ggcmi" #input("which planting? (ggcmi/dynamic): ")
if (planting_mode not in ["dynamic","ggcmi"]):
    raise ValueError("planting must either be 'ggcmi' or 'dynamic' ")	
#%% chill units 
def calc_chill_units(temp,chill_temp_max,accumulated_chill_units):
    #print(accumulated_chill_units)
    temp = max(temp,0) 
    if temp < chill_temp_max:
        chill_unit = min((chill_temp_max - temp)/chill_temp_max,1)
        #print("CU:",chill_unit) 
    else:
        chill_unit = 0
    new_chill = accumulated_chill_units + chill_unit
    #print("ACU:",new_chill)
    #np.append(chill_units.append(new_chill)
    return new_chill

#%% chill factor 
def calc_chill_fac_ldndc(agdd,gdd_flowering,chill_units,required_chill_units):
    gdd_vernalization = 0.5 * gdd_flowering   
    fchill = 1
    chill_influence = 0
    if (agdd > gdd_vernalization):
        #print("required chill:", required_chill)
        chill_influence = min((agdd - gdd_vernalization ) / gdd_vernalization,1)
 
        fchill = 1- chill_influence +  ( chill_units / required_chill_units)
    if fchill > 1:
        fchill = 1
    if fchill < 0:
        fchill = 0
    if (agdd > gdd_flowering):
        fchill = 1
    return fchill

#%% calculation of the temperature sums 
def calcGDD(ilat,ilon):
    # extract planting and harvest from crop calendar
    planting_doy  = cc_crop.sel(lat=ilat,lon=ilon).planting_day.values
    harvest_doy = cc_crop.sel(lat=ilat,lon=ilon).maturity_day.values
    if planting_mode == "dynamic":
        diff_doys = harvest_doy - planting_doy 
        planting_doys = dynamic_crop.sel(lat=ilat,lon=ilon).planting_day
        harvest_doys = dynamic_crop.sel(lat=ilat,lon=ilon).planting_day + diff_doys
    #get index for cell 
    spaceidx = ((scenic_df.lat == ilat) & (scenic_df.lon == ilon)).values
    scenic_slice = scenic_df[spaceidx].set_index("date")
    flowering_df = pd.DataFrame(index = year_vals)
    flowering_df[["lat","lon"]] = ilat,ilon
    # only process if there are values for that cell 
    if np.any(scenic_slice.tavg) & np.all(~(np.isnan((harvest_doy,planting_doy)))): 
        #iterate over each year in one cell to define the growth period of the plant 
        for year in tqdm(year_vals,desc=f"{ilat},{ilon}",file=sys.stdout): #
            if planting_mode == "dynamic":
                planting_doy = planting_doys.sel(year=year).values
                harvest_doy = harvest_doys.sel(year=year).values
            else:
                planting_doy = planting_doy#.values
                harvest_doy = harvest_doy#.values
            # convert the day of year to a date 
            planting_date = np.datetime64((datetime.datetime(year, 1, 1) +datetime.timedelta(planting_doy+0)).strftime("%Y-%m-%d"))
            # if the planting doy is larger than the harvest doy, this means the crop grows over the winter. 
            # the harvest date is therefore set to a year later
            if planting_doy < harvest_doy: 
                harvest_date = np.datetime64((datetime.datetime(year, 1, 1) +datetime.timedelta(harvest_doy+0)).strftime("%Y-%m-%d"))
            else: 
                harvest_date = np.datetime64((datetime.datetime(year+1, 1, 1) +datetime.timedelta(harvest_doy +0)).strftime("%Y-%m-%d"))
            # check if the date is still within the provided data 
            if  np.isin(np.datetime64(planting_date),scenic_slice.index): 
                # define the planting period
                plant_period = pd.date_range(planting_date,harvest_date)
                #iterate over the dates within the planting and sum up the GDDs 
                for date in plant_period:
                    if  ((np.datetime64(date)  >= planting_date) & (np.datetime64(date) < harvest_date) & np.isin(np.datetime64(date),scenic_slice.index)):
                            # get the cummulative temperature of the previous day
                            temp = scenic_slice.loc[date].tavg 
                            aGDD = scenic_slice.loc[date - np.timedelta64(1,"D"),"aGDD"]
                            fchill =  1
                            if dicts.crop_dict[crop]['vernalisation_flag'] == 1:
                                scenic_slice.loc[date,"achill"] = calc_chill_units(scenic_slice.loc[date].tavg,chill_temp_max,scenic_slice.loc[date-np.timedelta64(1,"D")].achill)
                                fchill = calc_chill_fac_ldndc(aGDD,dicts.crop_dict[crop]["gdd_flowering"],scenic_slice.loc[date].achill,req_chill_units)
                            
                            if (temp > tbase): 
                                if (temp > dicts.crop_dict[crop]['Tmax']):
                                    temp = dicts.crop_dict[crop]['Tmax']
                                # add the current daily average temperature - base temperature 
                                teff = temp - tbase
                                scenic_slice.loc[date,"aGDD"] = aGDD + teff * fchill
                            else: 
                                scenic_slice.loc[date,"aGDD"] = aGDD
                  # flowering things
                if  np.isin(np.datetime64(harvest_date),scenic_slice.index):
                    maturity_gdd = scenic_slice.loc[plant_period[0]:plant_period[-1]].aGDD.max()
                    if maturity_gdd < (dicts.crop_dict[crop]["gdd_maturity"]/2):
                        flowering_gdd,flowering_date,flowering_doy = np.nan,np.nan,np.nan
                    else:
                        flowering_gdd = dicts.crop_dict[crop]["gdd_flowering"]/dicts.crop_dict[crop]["gdd_maturity"]*maturity_gdd
                        flowering_date = scenic_slice.loc[plant_period[0]:plant_period[-1]].loc[scenic_slice.aGDD > flowering_gdd].index[0]
                        flowering_doy = flowering_date.dayofyear

                    #write to df 
                    flowering_df.loc[year+1,["flowering_gdd","flowering_doy"]] = [flowering_gdd,flowering_doy]
    return scenic_slice,flowering_df


#%%

if __name__ == "__main__":

	parser = argparse.ArgumentParser(description="tcrit arguments",
									 formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument("-c", "--crop", help="crop type (maize / winter_wheat / spring_wheat)")
	parser.add_argument("-s", "--scenario",help="scenario (f2017 / f2038/ f2065 / f2093)")
	parser.add_argument("-m", "--member", help = "Enemble member (E1 / E2 / E3 / E4 / E5)")
	parser.add_argument("-r", "--resolution", help="resolution of data (011/025)")
	parser.add_argument("-w", "--watering", help="watering method")
	args = parser.parse_args()

	print(f"start script {sys.argv[0]} at {datetime.datetime.now()}")
	start = time.time()
	#crop 
	#crop
	if args.crop is None: 
		crop = "winter_wheat"
		print("Warning: <cropname> missing, defaulting to winter_wheat")
	else:
		crop = args.crop
	print(f"Crop used: {crop}")

	#scenario
	if args.scenario is None: 
		scenario = 2017
		print("Warning: <scenario> missing, defaulting to 2017")
	else:
		scenario = args.scenario
	scenario = int(scenario)

	print(f"Forcing f{scenario}")
	
	# Ensemble Member
	if args.member is None:
		member = "E1"
		print("Warning: <member> missing, defaulting to E1")
	else:
		member = args.member

	print(f"Member {member}")
	#resolution 
	if args.resolution is None: 
		res = "025"
		print("No resolution specified, defaulting to 0.25°")
	else:
		res = args.resolution
		print(f"resolution specified {res}° x 10^(-2)")
		
	#watering 
	if args.watering is None: 
		watering = "rf"
		print("No watering method specified , defaulting to rainfed")
	else:
		watering = args.watering
		print(f"watering method specified {watering}")

	ggcmi_crop = dicts.crop_dict[crop]['GGCMI_crop_short']
	
	#scenic
	scenic_da = xr.open_dataset(f"{home.climatepath}/scenic_EU{res}/{member}_scenic_EU{res}_ssp370_f{scenario}_daily.nc")
	cc_crop = xr.open_dataset(f"{home.hspath}/crop_calendar/GGCMI_crop_calendars_EU{res}/{ggcmi_crop}_{watering}_ggcmi_crop_calendar_EU{res}.nc4")

	if member != "E1":
		print("adding spinup")
		spinup_da = xr.open_dataset(f"{home.climatepath}/scenic_EU{res}/spinup_f{scenario}.nc")
		scenic_da = spinup_da.combine_first(scenic_da)

	if planting_mode == "dynamic":
		dynamic_crop = xr.open_dataset(f"{home.hspath}/crop_calendar/planting_date_calculation/dynamic_planting_{crop}_f{scenario}.nc")


		
	#check if data has the same extend
	print(f"Using crop calendar: {ggcmi_crop}_{watering}_ggcmi_crop_calendar_EU{res}.nc4")
	print(f"Climate Lats        : {scenic_da.lat.values[0]}, {scenic_da.lat.values[len(scenic_da.lat)-1]}, Length: {len(scenic_da.lat)} Res: {scenic_da.lat.values[0]-scenic_da.lat.values[1]}")
	print(f"Crop Calendar Lats: {cc_crop.lat.values[0]}, {cc_crop.lat.values[len(cc_crop.lat)-1]}, Length: {len(cc_crop.lat)} Res: {cc_crop.lat.values[0]-cc_crop.lat.values[1]}")

	print(f"Climate Lons        : {scenic_da.lon.values[0]}, {scenic_da.lon.values[len(scenic_da.lon)-1]}, Length: {len(scenic_da.lon)} Res: {scenic_da.lon.values[-2]-scenic_da.lon.values[-1]}")
	print(f"Crop Calendar Lons: {cc_crop.lon.values[0]}, {cc_crop.lon.values[len(cc_crop.lon)-1]}, Length: {len(cc_crop.lon)} Res: {cc_crop.lon.values[-2]-cc_crop.lon.values[-1]}")

	#%% 
	timesteps = len(scenic_da.date)
	print(pd.unique(scenic_da.date.dt.year), "(",round(timesteps/365,2), "Years)")

	if mode == "test":
		if (res == "025"):
			lat_vals = [50.125, 50.375, 50.625, 50.875]
			lon_vals = [8.125, 8.375, 8.625, 8.875]
		if (res == "011"):
			lat_vals = [40.485 , 40.5961, 40.7072, 40.8183, 40.9294]
			lon_vals = [23.07  , 23.1813, 23.2926, 23.4039, 23.5152]
	elif mode == "run": 
		lat_vals =  scenic_da.lat.values
		lon_vals = scenic_da.lon.values

	latlon_list=[]

	for lat in lat_vals:
		for lon in lon_vals:
			latlon_list.append((lat,lon))

	
	cells  = len(lat_vals)*len(lon_vals)
	print(cells, "Cells") 
	
	scenic_da = scenic_da.sel(lat = slice(lat_vals[0],lat_vals[-1]),lon = slice(lon_vals[0],lon_vals[-1]))
	scenic_df = scenic_da.to_dataframe().reset_index() 
	year_vals = scenic_df.date.dt.year.unique()
	#Get Plant parameters 
	tbase = dicts.crop_dict[crop]['Tbase']
	if dicts.crop_dict[crop]['vernalisation_flag'] == 1:
		chill_temp_max = dicts.crop_dict[crop]['chill_temp_max']
		req_chill_units = dicts.crop_dict[crop]['chill_units']
		scenic_df[f"achill"] = 0 

	#create nan column for accumulated GDDs in dataframe 
	scenic_df["aGDD"] = 0
	#Cores 
	ncores = 128
	print(f"running on {ncores} Cores")
	
	# Create a pool with n=10 worker processes
	with Pool(ncores) as p:
		# The arguments are passed as tuples
		output_list = p.starmap(calcGDD, latlon_list)
		#GDD_list = p.starmap(calcGDD, tqdm(latlon_list,total = cells))
	print(f"loop of {ncores} finalized")
#	print(GDD_list) 
	p.close()
	p.join()
	print(f"finished iterations at {datetime.datetime.now()}") 

	## concattinate the spatial slices 
	GDD_list = [gdd_list[0] for gdd_list in output_list]
	GDD_df = pd.concat(GDD_list).reset_index()
	GDD_df.dropna(axis="index",subset=['date', 'lat','lon'],inplace=True) 
	#%% write dataframe to netcdf 
	date = time.strftime("%Y-%m-%d",time.gmtime())
	
	if planting_mode == "dynamic":
		output_gdd = f"{home.hsout}/dynamic_planting/{member}_scenic_EU{res}_f{scenario}_GDD_{crop}_{watering}"
		output_gdd = output_gdd + "_dynamic"
	else:
		output_gdd = f"{home.hsout}/ggcmi_planting/{member}_scenic_EU{res}_f{scenario}_GDD_{crop}_{watering}"
	#output_gdd = output_gdd + "_" + date
	output_gdd = hf.catch_filenames(output_gdd,mode)
	GDD_da = hf.dfToNetCDF(GDD_df,output_gdd)
	print(f"saved {output_gdd}.nc")

	if scenario == 2017:
		output_flowering = f"{home.hsout}/{member}_scenic_EU{res}_f{scenario}_flowering_{crop}_{watering}"
		if planting_mode == "dynamic":
			output_flowering = output_flowering + "_dynamic"
		#output_flowering = output_flowering + "_" + date
		output_flowering = hf.catch_filenames(output_flowering,mode)
		
		flowering_list = [f_list[1] for f_list in output_list]
		flowering_df = pd.concat(flowering_list).reset_index(names="date").rename(columns={"flowering_gdd":"flowering"})
		flowering_df.dropna(axis="index",subset=['date', 'lat','lon'],inplace=True)
		flowering_ds = hf.dfToNetCDF(flowering_df,output_flowering)
		print(f"saved {output_flowering}.nc")
		#
		#
		#hf.plotOverview(flowering_ds,GDD_da)
 
	end = time.time()
	print(f"finished script at {datetime.datetime.now()}")
	print(f"script ran for {round((end-start)/60,2)} minutes")